# standard imports
import json
import urllib2

# core Django imports
from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404, render_to_response

# local imports
import codepo.models


def search(request):
    postcode = urllib2.unquote(request.META["QUERY_STRING"])
    postcode = postcode.replace(" ", "").replace("+", "").upper()
    if len(postcode) not in (6, 7,):
        raise Http404
    split = len(postcode) - 3
    postcode = "%s %s" % (postcode[0:split], postcode[split:],)
    postcode = get_object_or_404(codepo.models.Postcode, postcode=postcode)
    county = postcode.ward.district.county
    data = {
        "postcode": postcode.postcode,
        "lon": postcode.lon,
        "lat": postcode.lat,
        "ward": postcode.ward.name,
        "district": postcode.ward.district.name,
        "county": (county and county.name) or None,
        }
    return HttpResponse(json.dumps(data), mimetype="application/json")


def markers(request):
    x1 = request.GET["x1"]
    y1 = request.GET["y1"]
    x2 = request.GET["x2"]
    y2 = request.GET["y2"]
    postcodes = codepo.models.Postcode.objects.filter(lat__gte=y1, lat__lte=y2, lon__gte=x1, lon__lte=x2)
    data = []
    for postcode in postcodes:
        data.append((postcode.postcode, postcode.lat, postcode.lon,))
    return HttpResponse(json.dumps(data), mimetype="application/json")
